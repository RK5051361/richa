package com.syntelinc.main;


public class Calculator implements ICalculator
{

	public int sum(int a, int b) {
		
		return a+b;
	}

	public int substration(int a, int b) {
		
		return a-b;
	}

	public int multiplication(int a, int b) {
		// TODO Auto-generated method stub
		return a*b;
	}

	public int division(int a, int b) throws Exception {
		if(b==0) {
			throw new Exception("undivisiable");
		}
		return a/b;
	}

	public boolean equalInteger(int a, int b) {
		boolean result = false;
		if(a==b) {
			result = true;
		}
		return result;
	}
   
}
